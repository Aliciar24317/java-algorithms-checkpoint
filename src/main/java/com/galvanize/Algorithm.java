package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {

    public boolean allEqual(String input) {
        boolean isEqual = true;

        if (input.isEmpty()) {
            isEqual = false;
            return isEqual;
        }
        String lowerCase = input.toLowerCase();
        char temp = lowerCase.charAt(0);
        for (int i = 0; i < lowerCase.length(); i++) {

            if (temp != lowerCase.charAt(i)) {
                isEqual = false;
                return isEqual;
            }else continue;
        }
        return isEqual;
    }

    public Map<String, Long> letterCount(String input){
        HashMap<String, Long> letterCount = new HashMap<>();

        if(input.isEmpty()){
            return letterCount;
        }
        String lowerCase = input.toLowerCase();
        for (int i = 0; i < lowerCase.length(); i++) {
            Long count = 0L;
            for (int j = 0; j < lowerCase.length(); j++) {
                if(lowerCase.charAt(i) == lowerCase.charAt(j)){
                    count++;
                }
                letterCount.put(String.valueOf(lowerCase.charAt(i)), count);
            }
        }
        return letterCount;
    }

    public String interleave(List<String> list1, List<String> list2){

        StringBuilder zipper = new StringBuilder("");

        if(list1.isEmpty() || list2.isEmpty()){
            return zipper.toString();
        }
        for (int i = 0; i < list1.size(); i++) {
            zipper.append(list1.get(i));
            zipper.append(list2.get(i));

        }
        return zipper.toString();
    }
}
