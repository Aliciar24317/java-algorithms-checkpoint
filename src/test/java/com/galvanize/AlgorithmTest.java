package com.galvanize;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {

    @Test
    public void defineMethodCalledAllEqualAndReturnsTrueAllLettersSame(){
        Algorithm myAlgorithm = new Algorithm();
        boolean result = myAlgorithm.allEqual("aAa");
        boolean result2 = myAlgorithm.allEqual("bbBbabbb");
        boolean result3 = myAlgorithm.allEqual("");
        assertTrue(result);
        assertFalse(result2);
        assertFalse(result3);
    }

    @Test
    public void letterCountShouldReturnMapOfCharactersAndCount(){
        Algorithm myAlgorithm = new Algorithm();
        Map<String, Long> result = myAlgorithm.letterCount("aa");
        Map<String, Long> result2 = myAlgorithm.letterCount("abBcd");
        Map<String, Long> result3 = myAlgorithm.letterCount("");

        assertEquals("{a=2}", result.toString());
        assertEquals("{a=1, b=2, c=1, d=1}", result2.toString());
        assertEquals("{}", result3.toString());
    }

    @Test
    public void interLeaveMethodShouldZipperLetters(){
        Algorithm myAlgorithm = new Algorithm();
        String result2 =  myAlgorithm.interleave(Arrays.asList("a", "c", "e"), Arrays.asList("b", "d", "f"));  // => returns "abcdef"
        String result3 = myAlgorithm.interleave(Collections.emptyList(), Collections.emptyList());
        String result = myAlgorithm.interleave(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));
        assertEquals("adbecf", result);
        assertEquals("abcdef", result2);
        assertEquals("", result3);
    }



}
